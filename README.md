# ABP Light IRC

This plugins adds a chat page to let your user connecting to your IRC channel, whith the flash lightIRC interface.

**Important - Required**
Since version 2.0, lightIRC is no more in the plugin package.

To use this plugin, you must:
* Download the last version of lightIRC at http://www.lightirc.com/download
* Unzip the archive you get and upload the lightIRC directory to your forum root

# INSTALLATION
You must have installed lightIRC
* Unpack the archive
* Copy the content the UPLOAD directory in your forum's root directory.
* Install and activate the plugin from the ACP
* Configure the plugin

# UPGRADE
* Update your lightIRC version
* Uninstall completely the previous plugin
* Install the new one